user_pref("browser.aboutConfig.showWarning", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.tabs.closeWindowWithLastTab", false);
user_pref("browser.toolbars.bookmarks.visibility", "never");
user_pref("network.proxy.allow_hijacking_localhost", true); // localhost through SOCKS proxy
user_pref("signon.rememberSignons", false);
