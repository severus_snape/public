{
  packageOverrides = pkgs: with pkgs; {
    myPackages = pkgs.buildEnv {
      name = "my-packages";
      paths = [
        # (import <nixgl> {}).auto.nixGLDefault # nvidia fix
        firefox
        josm
        mozlz4a
        passff-host
      ];
    };
  };
}
